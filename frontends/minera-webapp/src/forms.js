module.exports = {
	ExtractValuesFromForm( node , obj ){
		const r = (obj instanceof Object) ? Object.assign({}, obj) : {};
		if( !node ) return r;

		let elements;
		if( node instanceof Event){
			elements = node.target.querySelectorAll('input,select,textarea');
		}else if( node instanceof HTMLFormElement ){
			elements = node.querySelectorAll('input,select,textarea');
		}else{
			return r;
		}

		for(let i = 0 ; i < elements.length ; i++) {
			if( elements[i].type === 'submit' || elements[i].tagName === 'BUTTON' ) continue;
			if( !elements[i].name ) continue;
			if( elements[i].tagName === 'SELECT' && elements[i].hasAttribute('multiple') ){
				let options = elements[i].querySelectorAll('option');
				r[elements[i].name] = options.filter( function( o ){ return o.selected } ).map( function( o ){ return o.value } );
			}else if( elements[i].type === 'checkbox' ){
				r[elements[i].name] = elements[i].checked;
			}else if( elements[i].type === 'number' ){
				r[elements[i].name] = parseFloat( elements[i].value );
			}else{
				r[elements[i].name] = elements[i].value;
			}
		}
		return r;
	},
	CleanDatasetFromVueEntries( dataset ){
		let r = {};
		for( let key in dataset ){
			if( !dataset.hasOwnProperty( key ) ) continue;
			if( key.indexOf('v-') !== -1 ) continue;
			r[key] = dataset[key]
		}
		return r;
	}
};
