export function isDevEnv() {
    return location.host.indexOf('lvh.me') !== -1 || location.host.indexOf('localhost') !== -1 || location.host.indexOf('127.0.0.1') !== -1;
}