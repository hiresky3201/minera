import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store';
import Axios from 'axios';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueTippy from "vue-tippy";

// -- Utilities
import {isDevEnv} from "@/helpers/utils";

// -- CSS Imports
import './assets/tailwindcss.css';
import 'tippy.js/themes/light.css';
import 'vuejs-noty-fa/dist/vuejs-noty-fa.css';

// -- Flags
Vue.config.productionTip = false

// -- API Gateways
if( isDevEnv() ){
    window.API_URL = `http://${location.hostname}:4444`;
    window.WEBSOCKET_URL = `ws://${location.hostname}:4444/hermes`;
}
else {
    window.API_URL = `http://api.buzzly.art`;
    window.WEBSOCKET_URL = `ws://155.138.231.48:4444/hermes`;
}

// -- Axios setup
Vue.prototype.$http = Axios;

Axios.defaults.withCredentials = true;
Axios.defaults.headers.common['Access-Control-Allow-Origin']= true;
Axios.defaults.headers.common['X-Client-Type']= 'MINERA_WEB';

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyA5lyMwG4wTQWjs4AZ6L2m2bc56XPdSSdM',
        libraries: 'places'
    },
});

Vue.use(VueTippy, {
    directive: "tippy", // => v-tippy
    flipDuration: 0,
});

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
