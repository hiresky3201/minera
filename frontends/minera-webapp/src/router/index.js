import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/router/views/Login";
import Home from "@/router/views/Home";
import Administration from "@/router/views/Administration";

Vue.use( VueRouter );

const routes = [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/administracion',
        name: 'administration',
        component: Administration
    }
];

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior: function(){ return { x: 0, y: 0 } }
});

export default router;