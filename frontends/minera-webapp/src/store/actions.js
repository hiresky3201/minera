import app from "@/store/actions/app";
import toast from "@/store/actions/toast";
import utils from "@/store/actions/utils";
import websocket from "@/store/actions/websocket";

export default {
    ...app,
    ...toast,
    ...utils,
    ...websocket
}