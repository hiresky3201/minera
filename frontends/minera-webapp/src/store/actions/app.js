import axios from "axios";
import {ErrorDispatcher, ModalBus} from "@/store/actions/utils";
import {ExtractValuesFromForm} from "@/forms";
import router from "@/router";
import CreateEmployeeModal from "@/components/modals/CreateEmployeeModal";
import CreateMaterialModal from "@/components/modals/CreateMaterialModal";
import CreateVehicleModal from "@/components/modals/CreateVehicleModal";
import CreateTrafficLightModal from "@/components/modals/CreateTrafficLightModal";

export default{
    FetchEmployee({commit, dispatch, state}){
        axios.get(`${window.API_URL}/recuperar-empleado`)
        .then( r => {
            commit('StateAssign', {employee: r.data.data});
            dispatch('ConnectWebsocket', r.data.data.id );
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    ConnectWebsocket({dispatch}, employeeId ){
        window.CreateWebSocket = function () {
            try {
                console.log('Creando websocket');

                window._WebSocket = new WebSocket(`${window.WEBSOCKET_URL}?idSession=${employeeId}`);
                window._WebSocket.onopen = function () {
                    console.info("Conexion con hermes establecida");
                };
                window._WebSocket.onmessage = function (e) {
                    if (!e.data || !e.data.length) return;
                    dispatch('OnMessage', JSON.parse(e.data));
                };
                window._WebSocket.onclose = function () {
                    setTimeout(window.CreateWebSocket, 50);
                };

                // -- Create the websocket heartbeat
                // This is because if no message is sent within the next couple minutes,
                // the websocket connection is closed forcefully to prevent leaking connections
                clearInterval(window._WebSocketHeartBeat);
                window._WebSocketHeartBeat = setInterval(function () {
                    window._WebSocket.send("");
                }, 45000);
            } catch (ex) {
                console.log(ex);
            }
        };

        window.CreateWebSocket();
    },
    Login({commit, dispatch}, e){
        axios.post(`${window.API_URL}/iniciar-sesion`, ExtractValuesFromForm( e.target ))
        .then( r => {
            commit('StateAssign', { employee: r.data.data });
            dispatch('ConnectWebsocket', r.data.data.id );
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            router.push('/');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    Logout({commit, dispatch}, e){
        axios.post(`${window.API_URL}/cerrar-sesion`, ExtractValuesFromForm( e.target ))
        .then( r => {
            commit('StateAssign', { employee: null });
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            router.push('/login');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    CreateEmployee({commit, dispatch}, e){
        axios.post(`${window.API_URL}/empleados/crear`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchEmployeeList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    EditEmployee({commit, dispatch}, e){
        axios.post(`${window.API_URL}/empleados/editar`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchEmployeeList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    DeleteEmployee({commit, dispatch}, employeeId){
        axios.post(`${window.API_URL}/empleados/eliminar`, { 'id': employeeId })
        .then( r => {
            dispatch('FetchEmployeeList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    CreateMaterial({commit, dispatch}, e){
        axios.post(`${window.API_URL}/materiales/crear`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchMaterialList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    EditMaterial({commit, dispatch}, e){
        axios.post(`${window.API_URL}/materiales/editar`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchMaterialList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    DeleteMaterial({commit, dispatch}, materialId){
        axios.post(`${window.API_URL}/materiales/eliminar`, { 'id': materialId })
        .then( r => {
            dispatch('FetchMaterialList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    CreateVehicle({commit, dispatch}, e){
        axios.post(`${window.API_URL}/vehiculos/crear`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchVehicleList');
            dispatch('FetchVehiclesForMap');

            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    EditVehicle({commit, dispatch}, e){
        axios.post(`${window.API_URL}/vehiculos/editar`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchVehicleList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    DeleteVehicle({commit, dispatch}, materialId){
        axios.post(`${window.API_URL}/vehiculos/eliminar`, { 'id': materialId })
        .then( r => {
            dispatch('FetchVehicleList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    CreateTrafficLight({commit, dispatch}, e){
        axios.post(`${window.API_URL}/semaforos/crear`, ExtractValuesFromForm( e.target ))
        .then( r => {
            dispatch('FetchTrafficLightList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });

            ModalBus.$emit('close');
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    EditTrafficLight({commit, dispatch}, e){
        let data = ExtractValuesFromForm(e.target);
        axios.post(`${window.API_URL}/semaforos/editar`, data)
        .then( r => {
            try {
                dispatch('FetchTrafficLightList');
                dispatch('ShowToast', {text: 'Operacion Exitosa', type: 'success'});

                let websocketMessage = {
                    type: 'TRAFFIC_LIGHT_STATUS_UPDATE',
                    data: {
                        status: data.estado,
                        trafficLightId: data.id
                    }
                };

                window._WebSocket.send(JSON.stringify(websocketMessage));

                ModalBus.$emit('close');
            }catch( ex ){
                console.log( ex );
            }
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    DeleteTrafficLight({commit, dispatch}, materialId){
        axios.post(`${window.API_URL}/semaforos/eliminar`, { 'id': materialId })
        .then( r => {
            dispatch('FetchTrafficLightList');
            dispatch('ShowToast', { text: 'Operacion Exitosa', type: 'success' });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    FetchEmployeeList({commit, dispatch}){
        axios.get(`${window.API_URL}/empleados/lista`)
        .then( r => {
            commit('StateAssign', { employeeList: r.data.data });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    FetchMaterialList({commit, dispatch}){
        axios.get(`${window.API_URL}/materiales/lista`)
        .then( r => {
            commit('StateAssign', { materialList: r.data.data });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    FetchVehicleList({commit, dispatch}){
        axios.get(`${window.API_URL}/vehiculos/lista`)
        .then( r => {
            commit('StateAssign', { vehicleList: r.data.data });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    FetchTrafficLightList({commit, dispatch}){
        axios.get(`${window.API_URL}/semaforos/lista`)
        .then( r => {
            commit('StateAssign', { trafficLightList: r.data.data });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    FetchVehiclesForMap({commit, dispatch}){
        axios.get(`${window.API_URL}/vehiculos/lista`)
        .then( r => {
            commit('StateAssign', { mapVehicles: r.data.data });
        } )
        .catch( ErrorDispatcher( dispatch ) );
    },
    OpenCreateEmployeeModal(){
        ModalBus.$emit('open', { name: 'CreateEmployeeModal', component: CreateEmployeeModal })
    },
    OpenEditEmployeeModal( {state}, employeeId ){
        let targetEmployee = this.state.employeeList.find( e => e.id === employeeId );
        ModalBus.$emit('open', { name: 'CreateEmployeeModal', component: CreateEmployeeModal, props:{ employee: targetEmployee } })
    },
    OpenCreateMaterialModal(){
        ModalBus.$emit('open', { name: 'CreateMaterialModal', component: CreateMaterialModal })
    },
    OpenEditMaterialModal( {state}, materialId ){
        let targetMaterial = this.state.materialList.find( m => m.id === materialId );
        ModalBus.$emit('open', { name: 'CreateMaterialModal', component: CreateMaterialModal, props:{ material: targetMaterial } })
    },
    OpenCreateVehicleModal(){
        ModalBus.$emit('open', { name: 'CreateVehicleModal', component: CreateVehicleModal })
    },
    OpenEditVehicleModal( {state}, vehicleId ){
        let targetVehicle = this.state.vehicleList.find( m => m.id === vehicleId );
        ModalBus.$emit('open', { name: 'CreateVehicleModal', component: CreateVehicleModal, props:{ vehicle: targetVehicle } })
    },
    OpenCreateTrafficLightModal(){
        ModalBus.$emit('open', { name: 'CreateTrafficLightModal', component: CreateTrafficLightModal })
    },
    OpenEditTrafficLightModal( {state}, trafficLightId ){
        let targetTrafficLight = this.state.trafficLightList.find( m => m.id === trafficLightId );
        ModalBus.$emit('open', { name: 'CreateTrafficLightModal', component: CreateTrafficLightModal, props:{ trafficLight: targetTrafficLight } })
    },
}