import Vue from "vue";
import VueNoty from "vuejs-noty-fa";

Vue.use(VueNoty, {
    timeout: 4000,
    progressBar: true,
    layout: 'bottomRight',
    theme: 'sunset',
    icon: {
        success: ['fas', 'check'], // no icon
        info: ['fas', 'info-circle'],
        warning: ['fas', 'exclamation-triangle'],
        error: ['fas', 'exclamation-triangle']
    }
});

// -- For reference
let Notifier = Vue.prototype.$noty;
let WorkingToasts = [];

export default{
    /**
     * Shows a toast notification
     *
     * @param payload
     * @constructor
     */
    ShowToast({}, payload){
        let type = payload['type'];
        let text = payload['text'];

        Notifier[ type ]( text ? text : 'Operación exitosa' , payload )
    },
    /**
     * Shows a toast with close button
     *
     * @param payload
     * @constructor
     */
    ShowToastCloseButton({}, payload){
        let type = payload['type'];
        let text = payload['text'];
        Notifier[ type ]( text ? text : 'Operación exitosa' , {
            closeWith: ['button', 'click'],
            layout: 'bottomRight'
          })
        
    },
    /**
     * Shows a working toast, that is not able to be closed by user clicks
     * Created working toasts must have an id to be referenced when closing
     * To close this toast, CloseToast must be called with the given toastId
     *
     * @param payload
     * @constructor
     */
    ShowWorkingToast({}, payload){
        if( !payload['id'] ){
            console.error( 'Working toasts must be given an unique ID to be referenced when closing' );
            return;
        }

        // -- Ignore toasts that have an existing ID
        if( WorkingToasts.find( x => x.id === payload['id'] ) ) return;

        // -- Add toast to working stack
        WorkingToasts.push( Notifier[ 'info' ]( payload['text'] + '<i class="fas fa-spinner fa-spin ml-2"></i>', Object.assign( payload , { timeout: 0, closeWith:'none' } ) ) );
    },
    /**
     * Closes a working toast
     *
     * @param toastId
     * @constructor
     */
    CloseToast({}, toastId){
        let toast = WorkingToasts.find( x => x.id === toastId );
        if( !toast ) return;

        // -- Close toast
        toast.close();

        // -- Remove from working toasts
        WorkingToasts = WorkingToasts.splice( 1, WorkingToasts.indexOf( toast ) );
    },
    /**
     * Closes all working toasts
     *
     * @constructor
     */
    CloseWorkingToasts({}){
        WorkingToasts.forEach( x => x.close() );
        WorkingToasts = [];
    }
}