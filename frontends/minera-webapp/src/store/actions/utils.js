import Vue from 'vue'
import router from "@/router";

// -- Used to manage modal events
export const ModalBus = new Vue();

// -- Used for UUID checkups
export const EMPTY_UUID = '00000000-0000-0000-0000-000000000000';

// -- Used to dispatch exception events
export const ErrorDispatcher = d => e => d('Exception',e && e.response);

export default {
    Exception( { commit, dispatch } , response ){
        let data = response && response.data;
        commit('StateAssign', {working: false});
        commit('StateAssign', {openingModal: false});

        // -- Close any processing toasts
        dispatch('CloseWorkingToasts');

        if( data ){
            if( Array.isArray( data.errores ) && data.errores.length > 0 ){
                dispatch('ShowToast', {type: 'error', text: data.errores[0]})
            }
        }

        if( response && response.status === 403 ){
            router.push('/login');
        }
    },
}