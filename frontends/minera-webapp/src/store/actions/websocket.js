
export default {
    OnMessage( { commit, state, dispatch }, message ){
        console.log( message )
        if( message.type === 'VEHICLE_GEOLOCATION_UPDATE') {
            console.log('Updating vehicle geolocation');
            commit('UpdateVehicleGeolocation', { vehicleId: message.data.vehicleId, latitude: message.data.latitude, longitude: message.data.longitude })
        }
        if( message.type === 'TRAFFIC_LIGHT_STATUS_UPDATE') {
            console.log('Updating traffic status');
            commit('UpdateTrafficLightStatus', { trafficLightId: message.data.trafficLightId, status: message.data.status })
        }
    }
}
