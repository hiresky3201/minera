import Vue from 'vue'

export default{
    StateAssign( state, payload ) {
        for( let key in payload ){
            if( !payload.hasOwnProperty( key ) ) continue;
            state[key] = payload[key];
        }
    },
    DeleteSubProperty( state , { property, subProperty } ){
        Vue.delete( state[property] , subProperty );
    },
    UpdateSubProperty( state , { property, subProperty , value } ){
        Vue.set( state[property] , subProperty , value );
    },
    UpdateVehicleGeolocation( state, { vehicleId, latitude, longitude } ){
        let vehicle = state.mapVehicles.find( v => v.id === vehicleId );

        if( !vehicle ) return;

        vehicle.latitud = latitude;
        vehicle.longitud = longitude;
        //Vue.set( state.mapVehicles, vehicleId, vehicle );
    },
    UpdateTrafficLightStatus( state, { trafficLightId, status } ){
        let trafficLight = state.trafficLightList.find( t => t.id === trafficLightId );

        if( !trafficLight ) return;

        trafficLight.estado = status;
        //Vue.set( state.mapVehicles, trafficLightId, trafficLight );
    }
}