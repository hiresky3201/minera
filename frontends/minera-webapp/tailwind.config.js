module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors:{
        'primary': '#1678c2',
        'secondary': '#185A7D',
        'accent': '#14C0D8',
        'teal': '#30d9c4',
        'turquoise': '#00df8f',
        'completed': '#5bd46d',
        'transparent-black-o8': 'rgba(0, 0, 0, 0.8)',
        'transparent-white-o8': 'rgba(255, 255, 255, 0.8)',
        'transparent-blue-o6': 'rgba(0, 94, 138, 0.6)',
        'transparent-blue-o9': 'rgba(0, 94,138,.9)'
      },
      width:{
        '48': '12rem',
        '60': '15rem',
        '80': '20rem',
        '90': '22.5rem',
        '100': '25rem'
      },
      inset: {
        '16': '4rem',
        '12': '3rem',
        '10': '2.5rem',
        '8': '2rem',
        '6': '1.5rem',
        '5': '1.25rem',
        '4': '1rem',
        '3': '0.75rem',
        '2': '0.5rem',
        '1': '0.25rem',
        '-1': '-0.25rem',
        '-2': '-0.5rem',
        '-3': '-0.75rem',
        '-4': '-1rem',
        '-5': '-1.25rem',
        '-6': '-1.5rem',
        '-8': '-2rem',
        '-10': '-2.5rem',
        '-12': '-3rem',
        '-16': '-4rem',
      },
      height:{
        '1/4': '25%',
        '32': '8rem',
        '48': '12rem',
        '60': '15rem',
        '70': '18.5rem',
        '80': '20rem',
        '90': '22.5rem',
        '100': '25rem'
      },
      minWidth:{
        '24': '6rem',
        '32': '8rem'
      },
      maxWidth:{
        '1/2': '50%',
        '32': '8rem',
        '48': '12rem',
        '60': '15rem'
      },
      maxHeight: {
        '0': '0',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        '4/5': '80%',
        '9/10': '90%',
        'full': '100%',
      },
      padding:{
        '.5': '0.125rem',
        '38': '9.5rem',
        '40': '10rem'
      }
    },
  },
  variants: {
    right: ['responsive'],
  },
  plugins: [],
}
