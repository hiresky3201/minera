let vehicles = {
    'c76be22f-73e0-4679-aab1-e283e075d284': {
        lat: 20.1537,
        lng: -111.002,
    },
    '77bca332-c43a-406a-8bdf-550a9426df6a': {
        lat: 20.1537,
        lng: -111.002,
    }
};

window.CreateWebSocket = function () {
    window._WS = new WebSocket(`ws://localhost:4444/hermes?idSession=1234567890`);
    window._WS.onopen = function () {
        console.info("Conexion con hermes establecida");
    };
    window._WS.onclose = function () {
        setTimeout(window.CreateWebSocket, 50);
    };

    clearInterval(window._WSSIM);
    window._WSSIM = setInterval(function () {
        simulateVehicleMovement();
    }, 1000);
};

window.CreateWebSocket();

let simulateVehicleMovement = function(){
    Object.keys( vehicles ).forEach( k => {
        try{
            let v = vehicles[k];

            v.lat += 5;
            v.lng += 5;

            let positionUpdateMsg = {
                type: 'VEHICLE_GEOLOCATION_UPDATE',
                data: {
                    vehicleId: k,
                    latitude: v.lat,
                    longitude: v.lng
                }
            }

            window._WS.send( JSON.stringify( positionUpdateMsg ) );
        }catch( e ){
            console.log( e );
        }
    } );
}