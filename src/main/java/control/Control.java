package control;

import com.fasterxml.jackson.core.type.TypeReference;
import entidades.*;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import persistencia.CloseableEntityManager;
import persistencia.HibernateUtil;
import spark.Request;
import spark.Response;
import spark.Spark;

import javax.naming.AuthenticationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebSocket
public class Control {
    private static final Map<String, Session> sesionesActivas = new ConcurrentHashMap<>();

    public static void main(String[] args){
        Spark.port( 4444 );

        // -- Encargado de escuchar por todos los eventos de WebSocket
        Spark.webSocket( "/hermes", Control.class );

        Spark.before(( req, res ) -> {
            req.session( true );
            res.type( "application/json" );

            // -- Para evitar problemas de CORS
            String origin = req.headers( "Origin" );

            res.header( "Access-Control-Allow-Origin" , origin );
            res.header( "Access-Control-Allow-Credentials" , "true" );
            res.header( "Access-Control-Allow-Methods" , "GET, POST, OPTIONS" );
            res.header( "Access-Control-Allow-Headers" , "Authorization,Access-Control-Allow-Origin,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,X-Client-Type" );
            res.header( "Access-Control-Expose-Headers" , "Content-Length,Content-Range" );

            // -- Cuando el browser hace una peticion, primero hace una peticion de OPTIONS
            // esta debe ser capturada por nosotros y regresar un codigo HTTP OK
            // Si NO, va a fallar la peticion subsecuente
            if( "OPTIONS".equals( req.requestMethod() ) ){
                res.header( "Content-Type", "text/plain; charset=utf-8" );
                Spark.halt( 204, "" );
            }
        } );

        // -- Si la API tira un error, se regresa un codigo 500 con un wrapper de respuesta
        Spark.exception( Exception.class, (e, request, response) -> {
            if( e instanceof AuthenticationException )
                response.status( 403 );
            else
                response.status(500);

            // -- Para debuggear
            e.printStackTrace();

            response.body( new RespuestaApi( e ).toString() );
        });

        Spark.post( "/iniciar-sesion", Control::iniciarSesion );
        Spark.post( "/cerrar-sesion", Control::cerrarSesion );
        Spark.get( "/recuperar-empleado", Control::recuperarEmpleado );

        // -- Rutas para todas las entidades
        Spark.path( "/semaforos", () -> {
            Spark.get( "/lista", Control::recuperarSemaforos );
            Spark.post( "/crear", Control::crearSemaforo );
            Spark.post( "/editar", Control::editarSemaforo );
            Spark.post( "/eliminar", Control::eliminarSemaforo );
        } );
        Spark.path( "/vehiculos", () -> {
            Spark.get( "/lista", Control::recuperarVehiculos );
            Spark.post( "/crear", Control::crearVehiculo );
            Spark.post( "/editar", Control::editarVehiculo );
            Spark.post( "/eliminar", Control::eliminarVehiculo );
        } );
        Spark.path( "/empleados", () -> {
            Spark.get( "/lista", Control::recuperarEmpleados );
            Spark.post( "/crear", Control::crearEmpleado );
            Spark.post( "/editar", Control::editarEmpleado );
            Spark.post( "/eliminar", Control::eliminarEmpleado );
        } );
        Spark.path( "/materiales", () -> {
            Spark.get( "/lista", Control::recuperarMateriales );
            Spark.post( "/crear", Control::crearMaterial );
            Spark.post( "/editar", Control::editarMaterial );
            Spark.post( "/eliminar", Control::eliminarMaterial );
        } );
    }

    public static RespuestaApi iniciarSesion(Request request, Response response) throws Exception{
        try(CloseableEntityManager entityManager = HibernateUtil.crearEntityManager()){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            String nombreUsuario = cuerpo.get( "nombreUsuario" ).toString();
            String password = cuerpo.get( "password" ).toString();

            Empleado empleado = (Empleado) entityManager.crearQueryNativo( "SELECT * FROM Empleados WHERE NombreUsuario = ?1", Empleado.class )
                    .setParameter( 1, nombreUsuario )
                    .getResultList()
                    .stream().findFirst().orElse( null );

            if( empleado == null ) throw new Exception( "El nombre de usuario o contrasena no son correctas" );
            if( !empleado.getPassword().equals( password ) ) throw new Exception( "El nombre de usuario o contrasena no son correctas" );

            response.header( "Set-Cookie", String.format("idEmpleado=%s; Max-Age=604800; Domain=.buzzly.art; Path=/; SameSite=Lax; HttpOnly;", empleado.getId() ) );

            return new RespuestaApi( empleado );
        }
    }

    public static RespuestaApi recuperarEmpleado(Request request, Response response) throws Exception {
        try(CloseableEntityManager entityManager = HibernateUtil.crearEntityManager()){
            String idEmpleado = request.cookie( "idEmpleado" );
            Empleado empleado = entityManager.getEntidadPorId( Empleado.class, idEmpleado );

            if( empleado == null ) throw new AuthenticationException( "No esta autorizado para acceder a esta ruta" );

            return new RespuestaApi( empleado );
        }
    }

    public static RespuestaApi cerrarSesion(Request request, Response response) throws Exception {
        request.session().invalidate();
        response.header( "Set-Cookie", "idEmpleado=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;domain=.buzzly.art" );

        return new RespuestaApi( "OK" );
    }

    public static RespuestaApi crearSemaforo(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Semaforo semaforo = new Semaforo();
            semaforo.setEstado( EstadoSemaforo.VERDE );
            semaforo.setNombre( cuerpo.get( "nombre" ).toString() );
            semaforo.setLatitud( Float.parseFloat( cuerpo.get( "latitud" ).toString() ) );
            semaforo.setLongitud( Float.parseFloat( cuerpo.get( "longitud" ).toString() ) );

            entityManager.persistir( semaforo );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi editarSemaforo(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Semaforo semaforo = entityManager.getEntidadPorId( Semaforo.class, cuerpo.get( "id" ).toString() );

            if( semaforo == null ) throw new Exception( "El semaforo con ese ID no existe" );

            if( cuerpo.containsKey( "nombre" ) ){
                semaforo.setNombre( cuerpo.get( "nombre" ).toString() );
            }
            if( cuerpo.containsKey( "latitud" ) ){
                semaforo.setLatitud( Float.parseFloat( cuerpo.get( "latitud" ).toString() ) );
            }
            if( cuerpo.containsKey( "longitud" ) ){
                semaforo.setLongitud( Float.parseFloat( cuerpo.get( "longitud" ).toString() ) );
            }
            if( cuerpo.containsKey( "estado" ) ){
                EstadoSemaforo estadoSemaforo = EstadoSemaforo.valueOf( cuerpo.get( "estado" ).toString() );
                semaforo.setEstado( estadoSemaforo );
            }

            entityManager.editar( semaforo );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi eliminarSemaforo(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            if( !cuerpo.containsKey( "id" ) ) throw new Exception( "Es necesario especificar un id" );

            entityManager.iniciarTransaccion();

            entityManager.borrarEntidadPorId( Semaforo.class, cuerpo.get( "id" ).toString() );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi recuperarSemaforos(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            List<Semaforo> semaforos = entityManager.obtenEntidades( Semaforo.class );
            return new RespuestaApi( semaforos );
        }
    }

    public static RespuestaApi crearVehiculo(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Vehiculo vehiculo = new Vehiculo();
            vehiculo.setNombre( cuerpo.get( "nombre" ).toString() );
            vehiculo.setLatitud( 0F );
            vehiculo.setLongitud( 0F );

            entityManager.persistir( vehiculo );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi editarVehiculo(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Vehiculo vehiculo = entityManager.getEntidadPorId( Vehiculo.class, cuerpo.get( "id" ).toString() );

            if( vehiculo == null ) throw new Exception( "El vehiculo con ese ID no existe" );

            if( cuerpo.containsKey( "latitud" ) ){
                vehiculo.setLatitud( Float.parseFloat( cuerpo.get( "latitud" ).toString() ) );
            }
            if( cuerpo.containsKey( "longitud" ) ){
                vehiculo.setLongitud( Float.parseFloat( cuerpo.get( "longitud" ).toString() ) );
            }
            if( cuerpo.containsKey( "nombre" ) ){
                vehiculo.setNombre( cuerpo.get( "nombre" ).toString() );
            }

            entityManager.editar( vehiculo );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi eliminarVehiculo(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            if( !cuerpo.containsKey( "id" ) ) throw new Exception( "Es necesario especificar un id" );

            entityManager.iniciarTransaccion();

            entityManager.borrarEntidadPorId( Vehiculo.class, cuerpo.get( "id" ).toString() );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi recuperarVehiculos(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            List<Vehiculo> vehiculos = entityManager.obtenEntidades( Vehiculo.class );
            return new RespuestaApi( vehiculos );
        }
    }

    public static RespuestaApi crearEmpleado(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Empleado empleado = new Empleado();
            empleado.setNombre( cuerpo.get( "nombre" ).toString() );
            empleado.setNombreUsuario( cuerpo.get( "nombreUsuario" ).toString() );
            empleado.setPassword( cuerpo.get( "password" ).toString() );
            empleado.setRol( cuerpo.get( "rol" ).toString() );

            entityManager.persistir( empleado );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi editarEmpleado(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Empleado empleado = entityManager.getEntidadPorId( Empleado.class, cuerpo.get( "id" ).toString() );

            if( empleado == null ) throw new Exception( "El empleado con ese ID no existe" );

            if( cuerpo.containsKey( "nombreUsuario" ) ){
                empleado.setNombreUsuario( cuerpo.get( "nombreUsuario" ).toString() );
            }
            if( cuerpo.containsKey( "password" ) ){
                empleado.setPassword( cuerpo.get( "password" ).toString() );
            }
            if( cuerpo.containsKey( "rol" ) ){
                empleado.setRol( cuerpo.get( "rol" ).toString() );
            }
            if( cuerpo.containsKey( "nombre" ) ){
                empleado.setNombre( cuerpo.get( "nombre" ).toString() );
            }

            entityManager.editar( empleado );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi eliminarEmpleado(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            if( !cuerpo.containsKey( "id" ) ) throw new Exception( "Es necesario especificar un id" );

            entityManager.iniciarTransaccion();

            entityManager.borrarEntidadPorId( Empleado.class, cuerpo.get( "id" ).toString() );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi recuperarEmpleados(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            List<Empleado> empleados = entityManager.obtenEntidades( Empleado.class );
            return new RespuestaApi( empleados );
        }
    }

    public static RespuestaApi crearMaterial(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Material material = new Material();
            material.setNombre( cuerpo.get( "nombre" ).toString() );

            entityManager.persistir( material );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi editarMaterial(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            entityManager.iniciarTransaccion();

            Material material = entityManager.getEntidadPorId( Material.class, cuerpo.get( "id" ).toString() );

            if( material == null ) throw new Exception( "El material con ese ID no existe" );

            if( cuerpo.containsKey( "nombre" ) ){
                material.setNombre( cuerpo.get( "nombre" ).toString() );
            }

            entityManager.editar( material );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi eliminarMaterial(Request request, Response response) throws Exception{
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            Map<String, Object> cuerpo = cuerpoComoMapa( request.body() );

            if( !cuerpo.containsKey( "id" ) ) throw new Exception( "Es necesario especificar un id" );

            entityManager.iniciarTransaccion();

            entityManager.borrarEntidadPorId( Material.class, cuerpo.get( "id" ).toString() );
            entityManager.commit();

            return new RespuestaApi( "OK" );
        }
    }

    public static RespuestaApi recuperarMateriales(Request request, Response response) throws Exception {
        try( CloseableEntityManager entityManager = HibernateUtil.crearEntityManager() ){
            List<Material> materiales = entityManager.obtenEntidades( Material.class );
            return new RespuestaApi( materiales );
        }
    }

    public static Map<String, Object> cuerpoComoMapa(String cuerpo) throws Exception{
        if (cuerpo == null || cuerpo.isEmpty()) return new HashMap<>();
        return RespuestaApi.CONVERTIDOR.readValue( cuerpo, new TypeReference<HashMap<String, Object>>() {} );
    }

    @OnWebSocketConnect
    public void clienteConectado(Session session){
        try{
            String query = session.getUpgradeRequest().getRequestURI().getQuery();

            // -- Parsear los parametros del query
            Map<String,String > parametros = new HashMap<>();

            if( query != null && query.contains("=") ){
                String[] fields = query.split("&");
                for (String field : fields ) {
                    String[] pair = field.split("=");
                    parametros.put( pair[0] , pair[1] );
                }
            }

            String id = parametros.get( "idSession" );
            sesionesActivas.put( id, session );
        }catch ( Exception ex ){
            ex.printStackTrace();
            session.close();
        }
    }

    @OnWebSocketMessage
    public void mensajeRecibido(Session session, String mensaje){
        // -- Mensajes vacios son mensajes para mantener la sesion activa
        if( mensaje == null || mensaje.isEmpty() ) return;

        sesionesActivas.forEach( (key, value) -> {
            try(CloseableEntityManager entityManager = HibernateUtil.crearEntityManager()){
                Map<String, Object> cuerpo = RespuestaApi.CONVERTIDOR.readValue( mensaje, new TypeReference<HashMap<String, Object>>() {} );
                Map<String, Object> data = (Map<String, Object>) cuerpo.get( "data" );

                entityManager.iniciarTransaccion();

                if( "TRAFFIC_LIGHT_STATUS_UPDATE".equals( cuerpo.get( "type" ) ) ){
                    String idSemaforo = (String) data.get( "trafficLightId" );
                    EstadoSemaforo estado = EstadoSemaforo.valueOf( ( String ) data.get( "status" ) );

                    Semaforo semaforo = entityManager.getEntidadPorId( Semaforo.class, idSemaforo );
                    semaforo.setEstado( estado );

                    entityManager.editar( semaforo );
                }
                else if( "VEHICLE_GEOLOCATION_UPDATE".equals( cuerpo.get( "type" ) ) ){
                    String idVehiculo = (String) data.get( "vehicleId" );
                    float lat = Float.parseFloat( data.get( "latitude" ).toString() );
                    float lng = Float.parseFloat( data.get( "longitude" ).toString() );

                    Vehiculo vehiculo = entityManager.getEntidadPorId( Vehiculo.class, idVehiculo );
                    vehiculo.setLatitud( lat );
                    vehiculo.setLongitud( lng );

                    entityManager.editar( vehiculo );
                }

                entityManager.commit();

                value.getRemote().sendString( mensaje );
            }catch ( Exception ex ){
                ex.printStackTrace();
                sesionesActivas.remove( key );
            }
        } );
    }
}
