package control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import control.serializadores.DeserializadorDeLocalDateTime;
import control.serializadores.SerializadorDeByteBuddy;
import control.serializadores.SerializadorDeListaHibernate;
import control.serializadores.SerializadorDeLocalDateTime;
import org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor;

import java.time.LocalDateTime;
import java.util.*;

public class RespuestaApi {
    public static final ObjectMapper CONVERTIDOR = new ObjectMapper();

    static {
        SimpleModule module = new SimpleModule();
        module.addSerializer( List.class, new SerializadorDeListaHibernate() );
        module.addSerializer( LocalDateTime.class, new SerializadorDeLocalDateTime() );
        module.addSerializer( ByteBuddyInterceptor.class, new SerializadorDeByteBuddy() );
        module.addDeserializer( LocalDateTime.class, new DeserializadorDeLocalDateTime() );
        CONVERTIDOR.registerModule( module );
    }

    private Object data;
    private String tipoError;
    private List<String> errores;
    private boolean isError;

    public RespuestaApi() {
        this.isError = false;
    }

    public RespuestaApi( Object data ) {
        this.data = data;
        this.isError = false;
    }

    public RespuestaApi( Throwable exception ) {
        this( exception , exception.getMessage() );
    }

    public RespuestaApi( Throwable exception , String message ) {
        this.errores = Arrays.asList( message == null ? "Ocurrio un error inesperado" : message );
        this.tipoError = exception.getClass().getCanonicalName();
        this.isError = true;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getTipoError() {
        return tipoError;
    }

    public void setTipoError(String tipoError) {
        this.tipoError = tipoError;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    @Override
    public String toString() {
        try {
            return CONVERTIDOR.writeValueAsString( this );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
