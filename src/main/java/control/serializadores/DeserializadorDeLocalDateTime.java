package control.serializadores;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.*;

public class DeserializadorDeLocalDateTime extends StdDeserializer<LocalDateTime> {
    private static final ZoneOffset ZONE_OFFSET = ZonedDateTime.now().getOffset();
    public DeserializadorDeLocalDateTime(){
        this( null );
    }
    public DeserializadorDeLocalDateTime(Class<LocalDateTime> t) {
        super(t);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String v = jsonParser.getValueAsString();
        Instant i = Instant.parse( v );
        return LocalDateTime.ofInstant( i , ZoneId.systemDefault() );
    }
}
