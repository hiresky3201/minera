package control.serializadores;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor;

import java.io.IOException;

public class SerializadorDeByteBuddy extends JsonSerializer<ByteBuddyInterceptor> {
    @Override
    @SuppressWarnings( "unchecked" )
    public void serialize(ByteBuddyInterceptor interceptor, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if( interceptor.isUninitialized() ){
            jsonGenerator.writeNull();
        }else{
            jsonGenerator.writeObject( interceptor.getImplementation() );
        }
    }

    @Override
    public Class<ByteBuddyInterceptor> handledType() {
        return ByteBuddyInterceptor.class;
    }
}