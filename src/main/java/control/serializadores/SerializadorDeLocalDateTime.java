package control.serializadores;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class SerializadorDeLocalDateTime extends StdSerializer<LocalDateTime> {
    public SerializadorDeLocalDateTime(){
        this( null );
    }
    public SerializadorDeLocalDateTime(Class<LocalDateTime> t) {
        super(t);
    }
    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if( localDateTime == null ){
            jsonGenerator.writeNull();
        }else{
            jsonGenerator.writeString( localDateTime.atZone( ZoneId.systemDefault() ).toInstant().toString() );
        }
    }
}