package entidades;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public abstract class Entidad implements Serializable {
    protected String id;
    protected LocalDateTime creada;
    protected LocalDateTime actualizada;
    protected LocalDateTime borrada;

    public String getId() { return this.id; }

    public LocalDateTime getCreada() { return this.creada == null ? LocalDateTime.now() : this.creada; }

    public void setId(String id) { this.id = id; }

    public void setCreada(LocalDateTime created) { this.creada = created; }

    public LocalDateTime getActualizada() { return this.actualizada == null ? LocalDateTime.now() : this.actualizada; }

    public void setActualizada(LocalDateTime updated) { this.actualizada = updated; }

    public LocalDateTime getBorrada() { return this.borrada; }

    public void setBorrada(LocalDateTime borrada) { this.borrada = borrada; }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Entidad)) return false;

        Entidad model = (Entidad) obj;
        if( this.id == null && model.id != null ) return false;
        if( this.id != null && model.id == null ) return false;
        if( this.id == null ) return false;

        return this.id.equalsIgnoreCase( model.id );
    }

    @Override
    public int hashCode() { return Objects.hashCode(id); }
}