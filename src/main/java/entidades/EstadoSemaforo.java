package entidades;

public enum EstadoSemaforo {
    VERDE,
    AMARILLO,
    ROJO,
    NO_FUNCIONAL
}
