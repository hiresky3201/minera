package entidades;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Materiales")
public class Material extends Entidad {
    private String nombre;

    @Id
    @Override
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "ID", columnDefinition = "CHAR(36)")
    public String getId() { return id; }

    @Override
    @Column(name = "Creada" , nullable = false , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    public LocalDateTime getCreada() { return super.getCreada(); }

    @Override
    @Column(name = "Actualizada" , nullable = false , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    public LocalDateTime getActualizada() { return super.getActualizada(); }

    @Override
    @Column(name = "Borrada")
    public LocalDateTime getBorrada() { return super.getBorrada(); }

    @Column(name = "Nombre", nullable = false)
    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }
}
