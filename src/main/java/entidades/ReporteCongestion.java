package entidades;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "ReportesCongestion")
public class ReporteCongestion extends Entidad{
    private String comentarios;
    private List<Semaforo> semaforosInvolucrados;

    @Id
    @Override
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "ID", columnDefinition = "CHAR(36)")
    public String getId() { return id; }

    @Override
    @Column(name = "Creada" , nullable = false , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    public LocalDateTime getCreada() { return super.getCreada(); }

    @Override
    @Column(name = "Actualizada" , nullable = false , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    public LocalDateTime getActualizada() { return super.getActualizada(); }

    @Override
    @Column(name = "Borrada")
    public LocalDateTime getBorrada() { return super.getBorrada(); }

    @ManyToMany(targetEntity = Semaforo.class , fetch = FetchType.EAGER , cascade = CascadeType.ALL )
    @JoinTable(name="ReportesCongestionSemaforosInvolucrados",joinColumns = { @JoinColumn(name="SemaforoID") }, inverseJoinColumns = { @JoinColumn(name="ReporteCongestionID") })
    public List<Semaforo> getSemaforosInvolucrados() { return semaforosInvolucrados; }

    public void setSemaforosInvolucrados(List<Semaforo> semaforosInvolucrados) { this.semaforosInvolucrados = semaforosInvolucrados; }

    @Column(name = "Comentarios")
    public String getComentarios() { return comentarios; }

    public void setComentarios(String comentarios) { this.comentarios = comentarios; }
}
