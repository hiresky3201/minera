package entidades;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Semaforos")
public class Semaforo extends Entidad {
    private String nombre;
    private EstadoSemaforo estado;
    private float latitud;
    private float longitud;

    @Id
    @Override
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(generator = "uuid2")
    @Column(name = "ID", columnDefinition = "CHAR(36)")
    public String getId() { return id; }

    @Override
    @Column(name = "Creada" , nullable = false , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    public LocalDateTime getCreada() { return super.getCreada(); }

    @Override
    @Column(name = "Actualizada" , nullable = false , columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    public LocalDateTime getActualizada() { return super.getActualizada(); }

    @Override
    @Column(name = "Borrada")
    public LocalDateTime getBorrada() { return super.getBorrada(); }

    @Column(name = "Nombre", nullable = false)
    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    @Enumerated( EnumType.STRING )
    @Column(name = "Estado", nullable = false)
    public EstadoSemaforo getEstado() { return estado; }

    public void setEstado(EstadoSemaforo estado) { this.estado = estado; }

    @Column(name = "Latitud", nullable = false)
    public float getLatitud() { return latitud; }

    public void setLatitud(float latitud) { this.latitud = latitud; }

    @Column(name = "Longitud", nullable = false)
    public float getLongitud() { return longitud; }

    public void setLongitud(float longitud) { this.longitud = longitud; }

    @Transient
    @JsonGetter("estado")
    public String estadoComoString(){
        return this.estado.name();
    }
}
