package persistencia;

import entidades.Entidad;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Closeable;
import java.time.LocalDateTime;
import java.util.*;

public class CloseableEntityManager implements Closeable {
    private SessionFactory sessionFactory;
    private EntityManager entityManager;

    public CloseableEntityManager( SessionFactory sessionFactory ) {
        this.sessionFactory = sessionFactory;
        this.entityManager = sessionFactory.createEntityManager();
        this.entityManager.unwrap( Session.class ).setHibernateFlushMode( FlushMode.ALWAYS );
    }

    public EntityTransaction getTransaccion() {
        return this.entityManager.getTransaction();
    }

    public CloseableEntityManager iniciarTransaccion() {
        if ( this.getTransaccion().isActive() ) return this;
        this.getTransaccion().begin();
        return this;
    }

    public void commit() {
        if ( !this.getTransaccion().isActive() ) return;
        this.getTransaccion().commit();
    }

    public void commitRefrescar() {
        if ( this.getTransaccion().isActive() ) {
            this.getTransaccion().commit();
        }

        this.getEntityManager().close();

        this.entityManager = this.sessionFactory.createEntityManager();
    }

    public EntityManager getEntityManager() { return entityManager; }

    public void setEntityManager( EntityManager entityManager ) { this.entityManager = entityManager; }

    public <T extends Entidad> List<T> obtenEntidades(Class<T> className) {
        List<T> list;
        list = this.entityManager.createQuery( String.format( "from %s WHERE Borrada IS NULL" , className.getName() ) , className ).getResultList();
        return list == null ? new ArrayList<>() : list;
    }

    @SuppressWarnings( "unchecked" )
    public <T extends Entidad> T getEntidadPorId( Class<T> className , String id ) {
        if ( id == null || id.length() != 36 ) return null;
        try {
            return ( T ) this.entityManager.createNativeQuery( String.format( "SELECT * FROM %s WHERE ID = ?1" , getNombreTabla( className ) ) , className ).setParameter( 1 , id ).getSingleResult();
        } catch ( NoResultException e ) {
            return null;
        }
    }

    private <T extends Entidad> String getNombreTabla( Class<T> className ) {
        Table table = className.getAnnotation( Table.class );
        return table.name();
    }

    public <T extends Entidad> void borrarEntidadPorId( Class<T> className , String id ) {
        try {
            T entidad = this.entityManager.find( className , id );

            entidad.setBorrada( LocalDateTime.now() );
            this.entityManager.merge( entidad );
        } catch ( Exception e ) {
            if ( this.entityManager.getTransaction().isActive() ) this.entityManager.getTransaction().rollback();
            throw e;
        }
    }

    @SuppressWarnings( "unchecked" )
    public <T extends Entidad> List<T> obtenEntidades( Class<T> className , Collection<String> id ) {
        if ( id == null || id.isEmpty() ) return new ArrayList<>();
        Query entities = this.entityManager.createQuery(
                String.format( "SELECT t FROM %s t WHERE t.id in :ids" , className.getName() )
        ).setParameter( "ids" , id );
        return ( List<T> ) entities.getResultList();
    }

    public <T extends Entidad> Query crearQueryNativo( String query , Class<T> tClass ) {
        return this.entityManager.createNativeQuery( query , tClass );
    }

    @SuppressWarnings( "unchecked" )
    public <T extends Entidad> List<T> ejecutarQueryNativo( String query , Class<T> tClass ) {
        return ( List<T> ) this.entityManager.createNativeQuery( query , tClass ).getResultList();
    }

    public Query crearQueryNativo( String query ) {
        return this.entityManager.createNativeQuery( query );
    }

    public int ejecutarQueryActualizacion( String q , Object... objects ) throws Exception {
        Query query = this.entityManager.createNativeQuery( q );
        for ( int i = 0 ; i < objects.length ; i++ ) {
            if ( objects[ i ] == null ) throw new Exception("Numeros de parametros invalidos");
            query.setParameter( i + 1 , objects[ i ] );
        }
        return query.executeUpdate();
    }

    @Transactional
    public void editar( Entidad Entidad ) {
        this.entityManager.merge( Entidad );
    }

    @Transactional
    public void persistir( Entidad Entidad ) {
        this.entityManager.persist( Entidad );
    }

    @Override
    public void close() {
        if ( this.entityManager == null ) return;
        boolean isActive = this.entityManager.getTransaction().isActive();
        if ( isActive ) {
            this.entityManager.getTransaction().rollback();
        }
        this.entityManager.close();
    }
}
