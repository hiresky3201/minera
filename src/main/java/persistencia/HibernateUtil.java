package persistencia;

import entidades.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sessionFactory = null;

    static {
        try {
            Configuration configuration = new Configuration().configure();
            String connectionUrl = "jdbc:mysql://%s:3306/minera?useTimezone=true&serverTimezone=UTC&zeroDateTimeBehavior=convertToNull";

            String username = System.getenv( "MINERA_DB_USERNAME" );
            String password = System.getenv( "MINERA_DB_PASSWORD" );
            String dbHost = System.getenv( "MINERA_DB_HOST" );

            if( username == null ){
                username = "root";
            }
            if( password == null ){
                password = "sesamo";
            }
            if( dbHost == null ){
                dbHost = "localhost";
            }

            connectionUrl = String.format( connectionUrl, dbHost );

            configuration.setProperty( "hibernate.connection.url" , connectionUrl );
            configuration.setProperty( "hibernate.connection.username" , username );
            configuration.setProperty( "hibernate.connection.password" , password );
            configuration.setProperty( "show_sql" , "false" );
            configuration.setProperty( "hibernate.connection.autocommit" , "false" );
            configuration.setProperty( "org.hibernate.flushMode" , "ALWAYS" );

            // HikariCP settings

            // Maximum waiting time for a connection from the pool
            configuration.setProperty( "hibernate.hikari.connectionTimeout" , "30000" );
            // Minimum number of ideal connections in the pool
            //configuration.setProperty("hibernate.hikari.minimumIdle", "12"); Disabled to leave this to be managed by Hikari
            // Maximum number of actual connection in the pool
            configuration.setProperty( "hibernate.hikari.maximumPoolSize" , "5" );
            // Maximum time that a connection is allowed to sit ideal in the pool
            configuration.setProperty( "hibernate.hikari.idleTimeout" , "250000" );

            configuration.setProperty( "hibernate.hikari.leakDetectionThreshold" , "30000" );
            configuration.setProperty( "hibernate.hikari.maxLifetime" , "30000" );

            sessionFactory = configuration
                    .addAnnotatedClass( Empleado.class )
                    .addAnnotatedClass( Semaforo.class )
                    .addAnnotatedClass( Material.class )
                    .addAnnotatedClass( ReporteCongestion.class )
                    .addAnnotatedClass( Vehiculo.class )
                    .buildSessionFactory();
        } catch ( Exception e ) {
            System.err.println("Error al conectarse a la base de datos");
            e.printStackTrace();
            System.exit( 1 );
        }
    }

    public static CloseableEntityManager crearEntityManager() {
        return new CloseableEntityManager( getSessionFactory() );
    }

    private static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}